package com.powernote;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.powernote.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class IPInfo extends Activity {

	protected static final int RESULT_CLOSE_ALL = 2020;
	protected static final String CLIENT_ACTIVITY = "com.powernote.ControlPanelActivity";
	protected static final String HELP_ACTIVITY = "com.powernote.HelpActivity";
	//private static Button setServerIP;
	private static Button setServerIPIM;
	private static EditText serverIP;
	private static String serverIPAdd;
	public static String getServerIP(){
		return serverIPAdd;
	}
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.ipinfo);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//        WindowManager.LayoutParams.FLAG_FULLSCREEN);


		setServerIPIM = (Button) findViewById(R.id.buttonSetServerIP);
		//setServerIPIM = (ImageView) findViewById(R.id.imageSetIP);
		serverIP = (EditText) findViewById(R.id.serverIP);
		setServerIPIM.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				serverIPAdd = serverIP.getText().toString();

				Intent NextActivity = new Intent(CLIENT_ACTIVITY);
				startActivity(NextActivity);
//				ClientThread.setCommand("xcopy command C:\\ /s/t");
//				ClientThread.myThread = new Thread(new ClientThread());
//				ClientThread.myThread.start();
			}
		});
		Button setHelp = (Button) findViewById(R.id.buttonHelp);

		setHelp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	;

				Intent NextActivity = new Intent(HELP_ACTIVITY);
				startActivity(NextActivity);

			}
		});
		Button setAboutUs = (Button) findViewById(R.id.buttonAboutUS);

		setAboutUs.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getBaseContext(), "About Us\nPowerNote, a professional tool to enhance the functionality of your Smartphones.", Toast.LENGTH_LONG).show();

			}
		});
	}


}
