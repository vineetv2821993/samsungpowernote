package com.powernote;

import com.powernote.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

public class ControlPanelActivity extends Activity {
	
	public final String COMMAND_TOOL = "c:\\command\\nircmd ";
	
	public static ImageButton bMax,bComp, bClose, bPen, bText, bPlay, bRight, bLeft, bMouse; 
	public static boolean bMaxToggle,bCompToggle, bCloseToggle, bPenToggle, bTextToggle, bPlayToggle, bRightToggle, bLeftToggle, bMouseToggle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_control_panel);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//        WindowManager.LayoutParams.FLAG_FULLSCREEN);

		ClientThread.SERVER_IP = IPInfo.getServerIP();

		ClientThread.setCommand("c:\\command\\nircmd trayballoon \"Samsung Powernote Server\" \"Client is Connected\" \"shell32.dll,22\" 15000");				
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
		
	

		
		Toast.makeText(getBaseContext(), ClientThread.SERVER_IP, Toast.LENGTH_LONG).show();
		bComp  = (ImageButton) findViewById(R.id.imageButtonComp);
	    bCompToggle = true;
		bComp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				if(bCompToggle == true){
				bComp.setImageResource(R.drawable.computerglow);
				bCompToggle = false;
				}
				else{
					bComp.setImageResource(R.drawable.computer);
					bCompToggle = true;
				}				
				Intent NextActivity = new Intent("com.powernote.TouchScreenActivity");
				startActivity(NextActivity);
				

			}
		});		
		bMouse  = (ImageButton) findViewById(R.id.imageButtonMousePad);
	    bMouseToggle = true;
		bMouse.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				if(bMouseToggle == true){
				bMouse.setImageResource(R.drawable.mouseglow);
				bMouseToggle = false;
				}
				else{
					bMouse.setImageResource(R.drawable.mouse);
					bMouseToggle = true;
				}				
				Intent NextActivity = new Intent("com.powernote.SurfaceMouseActivity");
				startActivity(NextActivity);
				

			}
		});	
		bPen  = (ImageButton) findViewById(R.id.imageButtonPen);
	    bPenToggle = true;
		bPen.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				if(bPenToggle == true){
				bPen.setImageResource(R.drawable.gesture);
				bPenToggle = false;
				}
				else{
					bPen.setImageResource(R.drawable.gestureglow);
					bPenToggle = true;
				}				
				Intent NextActivity = new Intent("com.samsung.android.sdk.pen.pg.example5_6.PenSample5_6_TextRecognition");
				startActivity(NextActivity);
				

			}
		});	
		bRight  = (ImageButton) findViewById(R.id.imageButtonRight);
	    bRightToggle = true;
		bRight.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress right");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
			}
		});	
		bLeft  = (ImageButton) findViewById(R.id.imageButtonLeft);
	    bLeftToggle = true;
		bLeft.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress left");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
			}
		});	
		bPlay  = (ImageButton) findViewById(R.id.imageButtonPlay);
	    bPlayToggle = true;
		bPlay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress s");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
			}
		});	
		bMax  = (ImageButton) findViewById(R.id.imageButtonMax);
	    bMaxToggle = true;
		bMax.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress shift+F5");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
			}
		});	
		bClose  = (ImageButton) findViewById(R.id.imageButtonClose);
	    bCloseToggle = true;
		bClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress esc");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
			}
		});	
		
		bText  = (ImageButton) findViewById(R.id.imageButtonText);
	    bTextToggle = true;
		bText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				if(bTextToggle == true){
				bText.setImageResource(R.drawable.notext);
				bTextToggle = false;
				com.samsung.android.sdk.pen.pg.example5_6.PenSample5_6_TextRecognition.TextTransfer = false;
				}
				else{
					bText.setImageResource(R.drawable.text);
					bTextToggle = true;
					com.samsung.android.sdk.pen.pg.example5_6.PenSample5_6_TextRecognition.TextTransfer = true;
				}				
				

			}
		});	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.control_panel, menu);
		return true;
	}

}
