package com.samsung.android.sdk.pen.pg.tool;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pen.Spen;

public class SDKUtils {
    
    public static boolean processUnsupportedException(final Activity activity, 
            SsdkUnsupportedException e) {
        e.printStackTrace();
        int errType = e.getType();
        // 삼성폰이 아닐경우 혹은 Device 가 pen을 지원하지 않는 경우
        if(errType == SsdkUnsupportedException.VENDOR_NOT_SUPPORTED ||
                errType == SsdkUnsupportedException.DEVICE_NOT_SUPPORTED ) {
            Toast.makeText(activity, "This device does not support Spen.",
                    Toast.LENGTH_SHORT).show();
            activity.finish();
        } 
        else if( errType == SsdkUnsupportedException.LIBRARY_NOT_INSTALLED ) {
            // SpenSDK APK 가 설치되지 않은 경우
            showAlertDialog( activity, 
                "You need to install additional Spen software"
                +" to use this application."
                + "You will be taken to the installation screen."
                + "Restart this application after the software has been installed."
                , true);
        } else if( errType == SsdkUnsupportedException.LIBRARY_UPDATE_IS_REQUIRED ) {
            // SpenSDK APK 업그레이드가 반드시 필요한 경우
            showAlertDialog( activity,
                "You need to update your Spen software to use this application."
                + " You will be taken to the installation screen." 
                + " Restart this application after the software has been updated."
                , true);
        } else if( errType == SsdkUnsupportedException.LIBRARY_UPDATE_IS_RECOMMENDED ) {
            // SpenSDK APK 신규버전이 있어 업그레이드를 권장하는 경우
            showAlertDialog( activity,
                "We recommend that you update your Spen software"
                + " before using this application."
                + " You will be taken to the installation screen."
                + " Restart this application after the software has been updated."
                , false);
            return false; // 업데이트 진행하지 않는 경우, App 정상 동작 처리
        }
        return true;
    }

    private static void showAlertDialog(final Activity activity, String msg,
            final boolean closeActivity) {

        AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
        dlg.setIcon(activity.getResources().getDrawable(
            android.R.drawable.ic_dialog_alert));
        dlg.setTitle("Upgrade Notification")
            .setMessage(msg)
            .setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                        DialogInterface dialog, int which) {
                        // 마켓 사이트로 이동하여 APK 설치/업데이트
                        Uri uri = Uri.parse("market://details?id="
                                + Spen.SPEN_NATIVE_PACKAGE_NAME);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);

                        dialog.dismiss();
                        activity.finish();
                    }
                })
            .setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                        DialogInterface dialog, int which) {
                        if(closeActivity == true) {
                            // 설치하지 않을 경우 Activity 종료
                            activity.finish();
                        }
                        dialog.dismiss();
                    }
            })
            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if(closeActivity == true) {
                        // 설치하지 않을 경우 Activity 종료
                        activity.finish();
                    }
                }
            })
            .show();
        dlg = null;
    }
}
