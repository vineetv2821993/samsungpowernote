package com.samsung.android.sdk.pen.pg.example5_6;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.powernote.ClientThread;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pen.Spen;
import com.samsung.android.sdk.pen.SpenSettingPenInfo;
import com.samsung.android.sdk.pen.document.SpenNoteDoc;
import com.samsung.android.sdk.pen.document.SpenObjectBase;
import com.samsung.android.sdk.pen.document.SpenObjectTextBox;
import com.samsung.android.sdk.pen.document.SpenPageDoc;
import com.samsung.android.sdk.pen.engine.SpenContextMenuItemInfo;
import com.samsung.android.sdk.pen.engine.SpenControlListener;
import com.samsung.android.sdk.pen.engine.SpenSurfaceView;
import com.samsung.android.sdk.pen.pg.tool.SDKUtils;
import com.samsung.android.sdk.pen.recognition.SpenCreationFailureException;
import com.samsung.android.sdk.pen.recognition.SpenRecognitionBase.ResultListener;
import com.samsung.android.sdk.pen.recognition.SpenRecognitionInfo;
import com.samsung.android.sdk.pen.recognition.SpenTextRecognition;
import com.samsung.android.sdk.pen.recognition.SpenTextRecognitionManager;
import com.powernote.R;



@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
public class PenSample5_6_TextRecognition extends Activity {
	
	

	public final String COMMAND_TOOL = "c:\\command\\nircmd ";
	public static boolean TextTransfer  = true;

	
	
	final String LEFT = 33+".ppt";
	final String RIGHT = 34+".ppt";
	final String ZOOMIN = COMMAND_TOOL+"sendkeypress ctrl+plus";
	final String ZOOMOUT = COMMAND_TOOL+"sendkeypress ctrl+minus";
	
	final String BLACK = COMMAND_TOOL+"sendkeypress b";
	final String WHITE = COMMAND_TOOL+"sendkeypress w";
	final String VIEW_TASKBAR = COMMAND_TOOL+"sendkeypress ctrl+t";
	final String SHOW_CURSOR = COMMAND_TOOL+"sendkeypress ctrl+h";
	final String HIDE_CURSOR = COMMAND_TOOL+"sendkeypress esc";
	final String LOCK_SCREEN = COMMAND_TOOL+"sendkeypress rwin+l";
	final String SHUTDOWN = COMMAND_TOOL+"exitwin shutdownforce";
	final String WIDESCR = COMMAND_TOOL+"setdisplay 1366 768 32";
	final String FOURTHREE = COMMAND_TOOL+"setdisplay 1024 768 32";
	final String HD = COMMAND_TOOL+"setdisplay 1280 720 32";
	final String FULLHD = COMMAND_TOOL+"setdisplay 1920 1080 32";

	final String PAGE_JUMP = COMMAND_TOOL+"sendkeypress ";
	
	final String MUTE = COMMAND_TOOL+"mutesysvolume 1" ;
	final String UNMUTE = COMMAND_TOOL+"mutesysvolume 0" ;
	
	
	private boolean validCommand(String cMD) {
		// TODO Auto-generated method stub
		if (cMD.equals("<")){
			return true;
		}
		if (cMD.equals(">")){
			return true;
		}
		if (cMD.equals("+")){
			return true;
		}
		if (cMD.equals("-")){
			return true;
		}
		if (cMD.equals("B")){
			return true;
		}
		if (cMD.equals("U")){
			return true;
		}
		if (cMD.equals("T")){
			return true;
		}
		if (cMD.equals("[")){
			return true;
		}
		if (cMD.equals("]")){
			return true;
		}
		if (cMD.equals("#")){
			return true;
		}
		if (cMD.equals("?")){
			return true;
		}
		if (cMD.equals("4:3")){
			return true;
		}
		if (cMD.equals("16:9")){
			return true;
		}
		if (cMD.equals("FHD")){
			return true;
		}
		if (cMD.equals("HD")){
			return true;
		}
		if (cMD.equals("M")){
			return true;
		}
		if (cMD.equals("N")){
			return true;
		}
		if(cMD.charAt(0)=='F'&&cMD.length()<=3){
			try{
				
				int val = Integer.valueOf(cMD.substring(1));
				if( val>0&&val<=24){
					return true;
				}
				else return false;
			}catch(Exception e){
			return false;
			}
		}
		
		try{
			int val = Integer.valueOf(cMD);
			return true;
		}
		catch(Exception e){
			return false;
		}

	}
	
	private String mapCommand(String cMD) {
		if (cMD.equals("<")){
			return LEFT;
		}
		if (cMD.equals(">")){
			return RIGHT;
		}
		if (cMD.equals("+")){
			return ZOOMIN;
		}
		if (cMD.equals("-")){
			return ZOOMOUT;
		}
		if (cMD.equals("B")){
			return BLACK;
		}
		if (cMD.equals("U")){
			return WHITE;
		}
		if (cMD.equals("T")){
			return VIEW_TASKBAR;
		}
		if (cMD.equals("[")){
			return SHOW_CURSOR;
		}
		if (cMD.equals("]")){
			return HIDE_CURSOR;
		}
		if (cMD.equals("#")){
			return LOCK_SCREEN;
		}
		if (cMD.equals("?")){
			return SHUTDOWN;
		}
		if (cMD.equals("4:3")){
			return FOURTHREE;
		}
		if (cMD.equals("16:9")){
			return WIDESCR;
		}
		if (cMD.equals("FHD")){
			return FULLHD;
		}
		if (cMD.equals("HD")){
			return HD;
		}
		if (cMD.equals("M")){
			return MUTE;
		}
		if (cMD.equals("N")){
			return UNMUTE;
		}
		if(cMD.charAt(0)=='F'&&cMD.length()<=3){
			return PAGE_JUMP+cMD;
		}
		try{
			int val = Integer.valueOf(cMD);
			String PAGE = "";
			for(char x:cMD.toCharArray()){
				PAGE+= x+" ";
			}
			return PAGE_JUMP+PAGE+"enter";
		}
		catch(Exception e){
			return null;
		}
		// TODO Auto-generated method stub

	}

    private Context mContext;
    private SpenNoteDoc mSpenNoteDoc;
    private SpenPageDoc mSpenPageDoc;
    private SpenSurfaceView mSpenSurfaceView;
    RelativeLayout mSpenViewLayout;

    private SpenTextRecognition mTextRecognition = null;
    private SpenTextRecognitionManager mSpenTextRecognitionManager = null;
    private boolean mIsProcessingRecognition = false;

    private ImageView mSelectionBtn;
    private ImageView mPenBtn;

    private Rect mScreenRect;
    private int mToolType = SpenSurfaceView.TOOL_SPEN;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_recognition);
		Button backspace = (Button) findViewById(R.id.backspace); 
		backspace.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			ClientThread.setCommand(COMMAND_TOOL+"sendkeypress backspace");
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		Button enter = (Button) findViewById(R.id.buttonEnter); 
		enter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			ClientThread.setCommand(COMMAND_TOOL+"sendkeypress enter");
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		Button prevSlide = (Button) findViewById(R.id.left); 
		prevSlide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			ClientThread.setCommand(COMMAND_TOOL+"sendkeypress left");
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		Button nextSlide = (Button) findViewById(R.id.right); 
		nextSlide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(COMMAND_TOOL+"sendkeypress right");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}
		});
        mContext = this;

        // Spen ÃƒÂ¬Ã‚Â´Ã‹â€ ÃƒÂªÃ‚Â¸Ã‚Â°ÃƒÂ­Ã¢â€žÂ¢Ã¢â‚¬ï¿½
        boolean isSpenFeatureEnabled = false;
        Spen spenPackage = new Spen();
        try {
            spenPackage.initialize(this);
            isSpenFeatureEnabled = spenPackage.isFeatureEnabled(Spen.DEVICE_PEN);
        } catch (Exception e1) {
            Toast.makeText(mContext, "Cannot initialize Spen. SPen Software is not installed or Device doesnt support SPen.",
                Toast.LENGTH_LONG).show();
            e1.printStackTrace();
            finish();
        }

        mSpenViewLayout = (RelativeLayout) findViewById(R.id.spenViewLayout);

        // SpenView ÃƒÂ¬Ã†â€™Ã¯Â¿Â½ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â±
        mSpenSurfaceView = new SpenSurfaceView(mContext);
        if (mSpenSurfaceView == null) {
            Toast.makeText(mContext, "Cannot create new SpenSurfaceView.",
                Toast.LENGTH_SHORT).show();
            finish();
        }
        mSpenViewLayout.addView(mSpenSurfaceView);

        // ÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¨ÃƒÂ«Ã‚Â§Ã¯Â¿Â½ÃƒÂ¬Ã¯Â¿Â½Ã‹Å“ ÃƒÂ¬Ã…Â Ã‚Â¤ÃƒÂ­Ã¯Â¿Â½Ã‚Â¬ÃƒÂ«Ã‚Â¦Ã‚Â° ÃƒÂ¬Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¬Ã¯Â¿Â½Ã‚Â´ÃƒÂ¬Ã‚Â¦Ã‹â€ ÃƒÂ«Ã‚Â¥Ã‚Â¼ ÃƒÂªÃ‚ÂµÃ‚Â¬ÃƒÂ­Ã¢â‚¬Â¢Ã…â€œÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¤.
        Display display = getWindowManager().getDefaultDisplay();
        mScreenRect = new Rect();
        display.getRectSize(mScreenRect);
        // SpenNoteDoc ÃƒÂ¬Ã†â€™Ã¯Â¿Â½ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â±
        try {
            mSpenNoteDoc = new SpenNoteDoc(mContext,
                mScreenRect.width(), mScreenRect.height());
        } catch (IOException e) {
            Toast.makeText(mContext, "Cannot create new NoteDoc.",
                Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
        // NoteDocÃƒÂ¬Ã¢â‚¬â€�Ã¯Â¿Â½ÃƒÂ¬Ã¢â‚¬Å¾Ã…â€œ PageÃƒÂ«Ã‚Â¥Ã‚Â¼ ÃƒÂ¬Ã‚Â¶Ã¢â‚¬ï¿½ÃƒÂªÃ‚Â°Ã¢â€šÂ¬ÃƒÂ­Ã¢â‚¬Â¢Ã…â€œ ÃƒÂ­Ã¢â‚¬ÂºÃ¢â‚¬Å¾ ÃƒÂ¬Ã¯Â¿Â½Ã‚Â¸ÃƒÂ¬Ã…Â Ã‚Â¤ÃƒÂ­Ã¢â‚¬Å¾Ã‚Â´ÃƒÂ¬Ã…Â Ã‚Â¤ÃƒÂ«Ã‚Â¥Ã‚Â¼ ÃƒÂ¬Ã¢â‚¬â€œÃ‚Â»ÃƒÂ¬Ã¢â‚¬â€œÃ‚Â´ÃƒÂ¬Ã¢â€žÂ¢Ã¢â€šÂ¬ ÃƒÂ«Ã‚Â©Ã‚Â¤ÃƒÂ«Ã‚Â²Ã¢â‚¬Å¾ ÃƒÂ«Ã‚Â³Ã¢â€šÂ¬ÃƒÂ¬Ã‹â€ Ã‹Å“ÃƒÂ¬Ã¢â‚¬â€�Ã¯Â¿Â½ ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â¤ÃƒÂ¬Ã‚Â Ã¢â‚¬Â¢
        mSpenPageDoc = mSpenNoteDoc.appendPage();
        mSpenPageDoc.setBackgroundColor(0xFFD6E6F5);
        mSpenPageDoc.clearHistory();
        // PageDocÃƒÂ¬Ã¯Â¿Â½Ã¢â‚¬Å¾ ViewÃƒÂ¬Ã¢â‚¬â€�Ã¯Â¿Â½ ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â¤ÃƒÂ¬Ã‚Â Ã¢â‚¬Â¢
        mSpenSurfaceView.setPageDoc(mSpenPageDoc, true);

        initPenSettingInfo();
        // Listener ÃƒÂ«Ã¢â‚¬Å“Ã‚Â±ÃƒÂ«Ã‚Â¡Ã¯Â¿Â½
        mSpenSurfaceView.setControlListener(mControlListener);

        // Button ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â¤ÃƒÂ¬Ã‚Â Ã¢â‚¬Â¢
        mSelectionBtn = (ImageView) findViewById(R.id.selectionBtn);
        mSelectionBtn.setOnClickListener(mSelectionBtnClickListener);

        mPenBtn = (ImageView) findViewById(R.id.penBtn);
        mPenBtn.setOnClickListener(mPenBtnClickListener);

        selectButton(mPenBtn);

        setTextRecognition();

        if(isSpenFeatureEnabled == false) {
            mToolType = SpenSurfaceView.TOOL_FINGER;
            mSpenSurfaceView.setToolTypeAction(mToolType,
                SpenSurfaceView.ACTION_STROKE);
            Toast.makeText(mContext,
                "Device does not support Spen. \n You can draw stroke by finger",
                Toast.LENGTH_SHORT).show();
        }
    }

    private void initPenSettingInfo() {
        // Pen setting ÃƒÂ¬Ã‚Â´Ã‹â€ ÃƒÂªÃ‚Â¸Ã‚Â°ÃƒÂ­Ã¢â€žÂ¢Ã¢â‚¬ï¿½
        SpenSettingPenInfo penInfo = new SpenSettingPenInfo();
        penInfo.color = Color.BLUE;
        penInfo.size = 10;
        mSpenSurfaceView.setPenSettingInfo(penInfo);
    }

    private void setTextRecognition() {
        // TextRecognition ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â¤ÃƒÂ¬Ã‚Â Ã¢â‚¬Â¢
        mSpenTextRecognitionManager = new SpenTextRecognitionManager(mContext);

        List<SpenRecognitionInfo> textRecognitionList = mSpenTextRecognitionManager.getInfoList(
                SpenObjectBase.TYPE_STROKE, SpenObjectBase.TYPE_CONTAINER);

        try {
            if (textRecognitionList.size() > 0) {
                for (SpenRecognitionInfo info : textRecognitionList) {
                    if (info.name.equalsIgnoreCase("SpenText")) {
                        mTextRecognition = mSpenTextRecognitionManager
                            .createRecognition(info);
                        break;
                    }
                }
            } else {
                finish();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "SpenTextRecognitionManager class not found.",
                Toast.LENGTH_SHORT).show();
            return;
        } catch (InstantiationException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Failed to access the SpenTextRecognitionManager constructor.",
                Toast.LENGTH_SHORT).show();
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Failed to access the SpenTextRecognitionManager field or method.",
                Toast.LENGTH_SHORT).show();
            return;
        } catch (SpenCreationFailureException e) {
            // RecognitionÃƒÂ¬Ã¯Â¿Â½Ã¢â‚¬Å¾ ÃƒÂ¬Ã‚Â§Ã¢â€šÂ¬ÃƒÂ¬Ã¢â‚¬ÂºÃ¯Â¿Â½ÃƒÂ­Ã¢â‚¬Â¢Ã‹Å“ÃƒÂ¬Ã‚Â§Ã¢â€šÂ¬ ÃƒÂ¬Ã¢â‚¬Â¢Ã…Â ÃƒÂ«Ã…Â Ã¢â‚¬ï¿½ ÃƒÂªÃ‚Â¸Ã‚Â°ÃƒÂªÃ‚Â¸Ã‚Â°ÃƒÂ¬Ã¯Â¿Â½Ã‚Â¼ ÃƒÂªÃ‚Â²Ã‚Â½ÃƒÂ¬Ã…Â¡Ã‚Â° appÃƒÂ¬Ã¯Â¿Â½Ã¢â‚¬Å¾ ÃƒÂ«Ã‚Â¹Ã‚Â ÃƒÂ¬Ã‚Â Ã‚Â¸ÃƒÂ«Ã¢â‚¬Å¡Ã‹Å“ÃƒÂªÃ‚Â°Ã¢â‚¬Å¾ÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¤.
            e.printStackTrace();
            AlertDialog.Builder ad = new AlertDialog.Builder(this);
            ad.setIcon(this.getResources().getDrawable(
                android.R.drawable.ic_dialog_alert));
            ad.setTitle(this.getResources().getString(R.string.app_name))
                .setMessage(
                    "This device does not support Recognition.")
                .setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,
                            int which) {
                            // finish dialog
                            dialog.dismiss();
                            finish();
                        }
                    }).show();
            ad = null;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, "SpenTextRecognitionManager engine not loaded.",
                Toast.LENGTH_SHORT).show();
            return;
        }

        // ÃƒÂ¬Ã¯Â¿Â½Ã‚Â¸ÃƒÂ¬Ã¢â‚¬Â¹Ã¯Â¿Â½ÃƒÂ­Ã¢â‚¬Â¢Ã‚Â  ÃƒÂ¬Ã¢â‚¬â€œÃ‚Â¸ÃƒÂ¬Ã¢â‚¬â€œÃ‚Â´ÃƒÂ«Ã‚Â¥Ã‚Â¼ ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â¤ÃƒÂ¬Ã‚Â Ã¢â‚¬Â¢ÃƒÂ­Ã¢â‚¬Â¢Ã‚Â´ ÃƒÂ¬Ã‚Â¤Ã¢â€šÂ¬ÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¤. (kor, eng, chn) 
        List<String> languageList = mTextRecognition.getSupportedLanguage();
        if (textRecognitionList.size() > 0) {
            for (String language : languageList) {
                if (language.equalsIgnoreCase("kor")) {
                    mTextRecognition.setLanguage(language);
                    break;
                }
            }
        }

        try {
            mTextRecognition.setResultListener(new ResultListener() {
                @Override
                public void onResult(List<SpenObjectBase> input,
                    List<SpenObjectBase> output) {
                    // select ÃƒÂ«Ã¯Â¿Â½Ã…â€œ objectÃƒÂ«Ã¢â‚¬Å“Ã‚Â¤ÃƒÂ¬Ã¯Â¿Â½Ã‹Å“ rect ÃƒÂ«Ã‚Â²Ã¢â‚¬ï¿½ÃƒÂ¬Ã…â€œÃ¢â‚¬Å¾ÃƒÂ«Ã‚Â¥Ã‚Â¼ ÃƒÂªÃ‚ÂµÃ‚Â¬ÃƒÂ­Ã¢â‚¬Â¢Ã‚Â´ ÃƒÂ¬Ã¯Â¿Â½Ã‚Â¸ÃƒÂ¬Ã¢â‚¬Â¹Ã¯Â¿Â½ÃƒÂ«Ã¯Â¿Â½Ã…â€œ textÃƒÂªÃ‚Â°Ã¢â€šÂ¬ ÃƒÂªÃ‚Â·Ã‚Â¸ÃƒÂ«Ã‚Â Ã‚Â¤ÃƒÂ¬Ã‚Â§Ã‹â€  rect ÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â¤ÃƒÂ¬Ã‚Â Ã¢â‚¬Â¢ÃƒÂ­Ã¢â‚¬Â¢Ã…â€œÃƒÂ«Ã¢â‚¬â„¢Ã‚Â¤
                    // select ÃƒÂ«Ã¯Â¿Â½Ã…â€œ objectÃƒÂ«Ã‚Â¥Ã‚Â¼ ÃƒÂ¬Ã¢â‚¬Å¡Ã‚Â­ÃƒÂ¬Ã‚Â Ã…â€œÃƒÂ­Ã¢â‚¬Â¢Ã‹Å“ÃƒÂªÃ‚Â³Ã‚Â  ÃƒÂ¬Ã¯Â¿Â½Ã‚Â¸ÃƒÂ¬Ã¢â‚¬Â¹Ã¯Â¿Â½ ÃƒÂ«Ã¯Â¿Â½Ã…â€œ objectÃƒÂ«Ã‚Â¥Ã‚Â¼ pageDocÃƒÂ¬Ã¢â‚¬â€�Ã¯Â¿Â½ append ÃƒÂ­Ã¢â‚¬Â¢Ã‚Â´ÃƒÂ¬Ã‚Â¤Ã¢â€šÂ¬ÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¤.
                    RectF rect = new RectF(mScreenRect.width(),
                        mScreenRect.height(), 0, 0);

                    for (SpenObjectBase obj : input) {
                        if (rect.contains(obj.getRect()) == false) {
                            RectF objRect = obj.getRect();
                            rect.left = rect.left < objRect.left
                                ? rect.left : objRect.left;
                            rect.top = rect.top < objRect.top
                                ? rect.top : objRect.top;
                            rect.right = rect.right > objRect.right
                                ? rect.right : objRect.right;
                            rect.bottom = rect.bottom > objRect.bottom
                                ? rect.bottom : objRect.bottom;
                        }
                        mSpenPageDoc.removeObject(obj);
                    }

                    for (SpenObjectBase obj : output) {
                        if (obj instanceof SpenObjectTextBox) {
                            obj.setRect(rect, false);
                            //mSpenPageDoc.appendObject(obj);
                            String CMD = ((SpenObjectTextBox) obj).getText().toString();
                            if(TextTransfer == false){
                            if(validCommand(CMD)){
        						com.powernote.ClientThread.setCommand(mapCommand(CMD));
        						com.powernote.ClientThread.myThread = new Thread(new com.powernote.ClientThread());
        						com.powernote.ClientThread.myThread.start();
                            }else{
                    			
                            	Toast.makeText(mContext, "Unsupported Command",
                                        Toast.LENGTH_SHORT).show();
                            }
                            }else{
                       			String PAGE = "";
                    			for(char x:CMD.toCharArray()){
                    				if(x!=' '){PAGE+= x+" ";}
                    				else PAGE+="spc ";
                    				}
                    			
                    			com.powernote.ClientThread.setCommand(COMMAND_TOOL+"sendkeypress "+PAGE);
        						com.powernote.ClientThread.myThread = new Thread(new com.powernote.ClientThread());
        						com.powernote.ClientThread.myThread.start();
                            	Toast.makeText(mContext, CMD,
                                        Toast.LENGTH_SHORT).show();                          
                            }
                           /* Toast.makeText(mContext, ((SpenObjectTextBox) obj).getText().toString(),
                                    Toast.LENGTH_SHORT).show();
                        */
                        }
                    }
                    mIsProcessingRecognition = false;
                    mSpenSurfaceView.closeControl();
                    mSpenSurfaceView.update();
                }




            });
        } catch (IllegalStateException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "SpenTextRecognition is not loaded.",
                Toast.LENGTH_SHORT).show();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, "SpenTextRecognition is not loaded.",
                Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private final OnClickListener mSelectionBtnClickListener =
        new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectButton(mSelectionBtn);
                mSpenSurfaceView.setToolTypeAction(mToolType,
                    SpenSurfaceView.ACTION_SELECTION);
            }
        };

    private final OnClickListener mPenBtnClickListener =
        new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectButton(mPenBtn);
                mSpenSurfaceView.setToolTypeAction(mToolType,
                    SpenSurfaceView.ACTION_STROKE);
            }
        };

    private SpenControlListener mControlListener = new SpenControlListener() {
        
        @Override
        public boolean onCreated(ArrayList<SpenObjectBase> selectedList,
                ArrayList<Rect> arg1,
                ArrayList<SpenContextMenuItemInfo> arg2,
                ArrayList<Integer> arg3, int arg4, PointF arg5) {
            if (selectedList.size() > 0 && !mIsProcessingRecognition) {
                // select ÃƒÂ«Ã¯Â¿Â½Ã…â€œ storkeÃƒÂ«Ã‚Â¥Ã‚Â¼ listÃƒÂ¬Ã¢â‚¬â€�Ã¯Â¿Â½ ÃƒÂ«Ã¢â‚¬Å¾Ã‚Â£ÃƒÂ¬Ã¢â‚¬â€œÃ‚Â´ requestÃƒÂ«Ã‚Â¡Ã…â€œ ÃƒÂ«Ã¢â‚¬Å¡Ã‚Â ÃƒÂ«Ã‚Â Ã‚Â¤ÃƒÂ¬Ã‚Â¤Ã¢â€šÂ¬ÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¤
                ArrayList<SpenObjectBase> inputList = new ArrayList<SpenObjectBase>();
                for (int i = 0; i < selectedList.size(); i++) {
                    if (selectedList.get(i).getType() == SpenObjectBase.TYPE_STROKE) {
                        inputList.add(selectedList.get(i));
                    }
                }

                if (inputList.size() <= 0) {
                    return false;
                }
                mIsProcessingRecognition = true;
                try {
                    mTextRecognition.request(inputList);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "SpenTextRecognition is not loaded.",
                        Toast.LENGTH_SHORT).show();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "SpenTextRecognition engine not loaded.",
                        Toast.LENGTH_SHORT).show();
                    return false;
                }
                return true;
            }
            return false;
        }

        @Override
        public boolean onClosed(ArrayList<SpenObjectBase> arg0) {
            return false;
        }

        @Override
        public boolean onMenuSelected(ArrayList<SpenObjectBase> arg0,
                int arg1) {
            return false;
        }

        @Override
        public void onObjectChanged(ArrayList<SpenObjectBase> arg0) {
        }

        @Override
        public void onRectChanged(RectF arg0, SpenObjectBase arg1) {
        }

        @Override
        public void onRotationChanged(float arg0, SpenObjectBase arg1) {
        }
    };

    private void selectButton(View v) {
        // ÃƒÂ«Ã‚ÂªÃ‚Â¨ÃƒÂ«Ã¢â‚¬Å“Ã…â€œÃƒÂ¬Ã¢â‚¬â€�Ã¯Â¿Â½ ÃƒÂ«Ã¢â‚¬ï¿½Ã‚Â°ÃƒÂ«Ã¯Â¿Â½Ã‚Â¼ ÃƒÂ«Ã‚Â²Ã¢â‚¬Å¾ÃƒÂ­Ã…Â Ã‚Â¼ÃƒÂ¬Ã¯Â¿Â½Ã¢â‚¬Å¾ ÃƒÂ­Ã¢â€žÂ¢Ã…â€œÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â±ÃƒÂ­Ã¢â€žÂ¢Ã¢â‚¬ï¿½/ÃƒÂ«Ã‚Â¹Ã¢â‚¬Å¾ÃƒÂ­Ã¢â€žÂ¢Ã…â€œÃƒÂ¬Ã¢â‚¬Å¾Ã‚Â±ÃƒÂ­Ã¢â€žÂ¢Ã¢â‚¬ï¿½ ÃƒÂ¬Ã¢â‚¬Â¹Ã…â€œÃƒÂ­Ã¢â‚¬Å¡Ã‚Â¨ÃƒÂ«Ã¢â‚¬Â¹Ã‚Â¤.
        mSelectionBtn.setSelected(false);
        mPenBtn.setSelected(false);

        v.setSelected(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mTextRecognition != null) {
            mSpenTextRecognitionManager.destroyRecognition(mTextRecognition);
            mSpenTextRecognitionManager.close();
        }

        if(mSpenSurfaceView != null) {
            mSpenSurfaceView.closeControl();
            mSpenSurfaceView.close();
            mSpenSurfaceView = null;
        }

        if(mSpenNoteDoc != null) {
            try {
                mSpenNoteDoc.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mSpenNoteDoc = null;
        }
    }
}